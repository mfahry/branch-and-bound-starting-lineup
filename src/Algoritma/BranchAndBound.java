/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Algoritma;

import static DAA.DesainDanAnalisis.*;
import data.UBclass;
import data.dataPlayerBB;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Jati
 */
public class BranchAndBound {

    int gk = 3, df = 8, mf = 7, fw = 5,
            totalWeight = 11, totalWeightGK = 1, totalWeightDF = DF, totalWeightMF = MF, totalWeightFW = FW,
            countGoodGK = 0, countBadGK = 0, countAddGK = 0,
            countGoodDF = 0, countBadDF = 0, countAddDF = 0,
            countGoodMF = 0, countBadMF = 0, countAddMF = 0,
            countGoodFW = 0, countBadFW = 0, countAddFW = 0;
    public ArrayList<Integer> ub = new ArrayList<>();
    public ArrayList<UBclass> dataBB = new ArrayList<>();

    public void cekPlayerBB() {
//        try {
        StartTime = new Date().getTime();
        UBclass ubc = null;
        //mencatat fitnes tiap pemain
        for (int i = 0; i < 23; i++) {
            if (i < 3) {
                if (arrAllDataPlayerBB.get(i).getFitness().equals("G")) {
                    countGoodGK++;
                } else {
                    countBadGK++;
                }
            }
            if (i >= 3 && i < 11) {
                if (arrAllDataPlayerBB.get(i).getFitness().equals("G")) {
                    countGoodDF++;
                } else {
                    countBadDF++;
                }
            }
            if (i >= 11 && i < 18) {
                if (arrAllDataPlayerBB.get(i).getFitness().equals("G")) {
                    countGoodMF++;
                } else {
                    countBadMF++;
                }
            }
            if (i >= 18 && i < 23) {
                if (arrAllDataPlayerBB.get(i).getFitness().equals("G")) {
                    countGoodFW++;
                } else {
                    countBadFW++;
                }
            }
        }
        //SORTING GK
        if (countGoodGK == 3 || countBadGK == 3) {
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 0; i < 3; i++) {
                    arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                }
                //ub gk
                for (int i = 0; i < arrSortirDataPlayerBB.size(); i++) {
                    if (i == arrSortirDataPlayerBB.size() - 1) {
                        ub.add(arrSortirDataPlayerBB.get(i).getSkills());
                    } else {
                        ub.add(arrSortirDataPlayerBB.get(i).getSkills() + ((totalWeightGK - 1) * (arrSortirDataPlayerBB.get(i + 1).getDensity())));
                    }
                }
                for (int i = 0; i < arrSortirDataPlayerBB.size(); i++) {
                    dataBB.add(new UBclass(ub.get(i), arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                }
                ubc = Collections.max(dataBB, new Comparator<UBclass>() {
                    @Override
                    public int compare(UBclass o1, UBclass o2) {
                        return Double.compare(o1.getUpperBound(), o2.getUpperBound());
                    }
                });
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{ubc.getNo(), ubc.getNama(), ubc.getPosisi(), ubc.getSkills(), ubc.getFitness(), ubc.getDensity(), ubc.getUpperBound()});
            }

        } else {
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 0; i < gk; i++) {
                    if (arrAllDataPlayerBB.get(i).getFitness().equals("G")) {
                        arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                    }
                }
                //ub gk
                for (int i = 0; i < arrSortirDataPlayerBB.size(); i++) {
                    if (i == arrSortirDataPlayerBB.size() - 1) {
                        ub.add(arrSortirDataPlayerBB.get(i).getSkills());
                    } else {
                        ub.add(arrSortirDataPlayerBB.get(i).getSkills() + ((totalWeightGK - 1) * (arrSortirDataPlayerBB.get(i + 1).getDensity())));
                    }
                }
                for (int i = 0; i < arrSortirDataPlayerBB.size(); i++) {
                    dataBB.add(new UBclass(ub.get(i), arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                }

                ubc = Collections.max(dataBB, new Comparator<UBclass>() {
                    @Override
                    public int compare(UBclass o1, UBclass o2) {
                        return Double.compare(o1.getUpperBound(), o2.getUpperBound());
                    }
                });
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{ubc.getNo(), ubc.getNama(), ubc.getPosisi(), ubc.getSkills(), ubc.getFitness(), ubc.getDensity(), ubc.getUpperBound()});
            }
        }
        countGoodGK = 0;
        countBadGK = 0;
        arrSortirDataPlayerBB.clear();
        dataBB.clear();



        //SORTING DF
//        while (countAddDF < DF) {
        if (countGoodDF <= DF) {
            int pickedValue = 0, UB = 0, l, r;
            System.out.println("Count GOOD DF : " + countGoodDF);
            System.out.println("Needed DF : " + DF);

            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 3; i < 11; i++) {
                    arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getFitness().equals("G")) {
                            if (j == arrSortirDataPlayerBB.size() - 1) {
                                l = (pickedValue + arrSortirDataPlayerBB.get(j).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(j).getDensity());
                                r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(j).getDensity());

                            } else {
                                l = (pickedValue + arrSortirDataPlayerBB.get(j).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(j + 1).getDensity());
                                r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(j + 1).getDensity());
                            }
                            dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(j).getNo(), arrSortirDataPlayerBB.get(j).getSkills(), arrSortirDataPlayerBB.get(j).getNama(), arrSortirDataPlayerBB.get(j).getPosisi(), arrSortirDataPlayerBB.get(j).getFitness(), arrSortirDataPlayerBB.get(j).getDensity()));
                            countAddDF++;
                            totalWeightDF--;
                            System.out.println("Data ditambah di DATABB : " + arrSortirDataPlayerBB.get(j).getNama());
                            pickedValue += arrSortirDataPlayerBB.get(j).getSkills();
                            arrSortirDataPlayerBB.remove(j);
                        }
//                        else {
//                            countAddDF++;
//                            arrSortirDataPlayerBB.remove(j);
//                        }
                    }

                }
            }

//            for (int i = 0; i < dataBB.size() && countAddDF < DF; i++) {
//                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
//                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
//                System.out.println("ADDED : " + dataBB.get(i).getNama());
//            }


        }
//        }

        if (countGoodDF == 8 || countBadDF == 8) {
            int pickedValue = 0, l, r;
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 3; i < 11; i++) {
                    arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                }
            }
            //ub df
            for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddDF < DF; i++) {
                if (i == arrSortirDataPlayerBB.size() - 1) {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                    r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                } else {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                }
                if (r > l) {
                    //arrAllDataPlayerBB.remove(i);
                } else {
                    dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                    countAddDF++;
                    totalWeightDF--;
                    pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                }
            }

            for (int i = 0; i < dataBB.size(); i++) {
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
            }


            while (countAddDF < DF) {
                for (int i = 0; i < dataBB.size(); i++) {
                    String x = dataBB.get(i).getNama();
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getNama().equals(x)) {
                            arrSortirDataPlayerBB.remove(j);
                        }
                    }
                }

                dataBB.clear();
                for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddDF < DF; i++) {
                    if (i == arrSortirDataPlayerBB.size() - 1) {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                        r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                    } else {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                        r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    }
                    if (r > l) {
//                    System.out.println("MASUK R > L : " + arrSortirDataPlayerBB.get(i));
                        //arrSortirDataPlayerBB.remove(i);
                    } else {
                        dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                        countAddDF++;
                        totalWeightDF--;
                        pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                    }
                }

                for (int i = 0; i < dataBB.size(); i++) {
                    DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                    modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});

                }
            }

        } else {
            int pickedValue = 0, l, r;
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 3; i < 11; i++) {
                    if (arrAllDataPlayerBB.get(i).getFitness().equals("G")) {
                        arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                    }
                }
            }
            //ub df
            for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddDF < DF; i++) {
                if (i == arrSortirDataPlayerBB.size() - 1) {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                    r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                } else {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                }
                if (r > l) {
                    //arrAllDataPlayerBB.remove(i);
                } else {
                    dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                    countAddDF++;
                    totalWeightDF--;
                    pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                }
            }

            for (int i = 0; i < dataBB.size(); i++) {
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
            }

            while (countAddDF < DF) {
                for (int i = 0; i < dataBB.size(); i++) {
                    String x = dataBB.get(i).getNama();
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getNama().equals(x)) {

                            arrSortirDataPlayerBB.remove(j);
                        }
                    }
                }

                dataBB.clear();

                for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddDF < DF; i++) {
                    if (i == arrSortirDataPlayerBB.size() - 1) {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                        r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                    } else {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightDF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                        r = (pickedValue) + ((totalWeightDF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    }
                    if (r > l) {
//                    System.out.println("MASUK R > L : " + arrSortirDataPlayerBB.get(i));
                        //arrSortirDataPlayerBB.remove(i);
                    } else {
                        dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                        countAddDF++;
                        totalWeightDF--;
                        pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                    }
                }

                for (int i = 0; i < dataBB.size(); i++) {
                    DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                    modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
                }
            }


        }
        countGoodDF = 0;
        countBadDF = 0;
        arrSortirDataPlayerBB.clear();
        dataBB.clear();


        //SORTING MF
        if (countGoodMF <= MF) {
//            INI
            int pickedValue = 0, UB = 0, l, r;
            System.out.println("Count GOOD MF : " + countGoodMF);
            System.out.println("Needed MF : " + MF);

            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 11; i < 18; i++) {
                    arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getFitness().equals("G")) {
                            if (j == arrSortirDataPlayerBB.size() - 1) {
                                l = (pickedValue + arrSortirDataPlayerBB.get(j).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(j).getDensity());
                                r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(j).getDensity());

                            } else {
                                l = (pickedValue + arrSortirDataPlayerBB.get(j).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(j + 1).getDensity());
                                r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(j + 1).getDensity());
                            }
                            dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(j).getNo(), arrSortirDataPlayerBB.get(j).getSkills(), arrSortirDataPlayerBB.get(j).getNama(), arrSortirDataPlayerBB.get(j).getPosisi(), arrSortirDataPlayerBB.get(j).getFitness(), arrSortirDataPlayerBB.get(j).getDensity()));
                            countAddMF++;
                            totalWeightMF--;
                            System.out.println("Data ditambah di DATABB : " + arrSortirDataPlayerBB.get(j).getNama());
                            pickedValue += arrSortirDataPlayerBB.get(j).getSkills();
                            arrSortirDataPlayerBB.remove(j);
                        }
//                        else {
//                            countAddDF++;
//                            arrSortirDataPlayerBB.remove(j);
//                        }
                    }

                }
            }

            for (int i = 0; i < dataBB.size(); i++) {
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
                System.out.println("ADDED : " + dataBB.get(i).getNama());
            }
            while (countAddMF < MF) {
                for (int i = 0; i < dataBB.size(); i++) {
                    String x = dataBB.get(i).getNama();
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getNama().equals(x)) {

                            arrSortirDataPlayerBB.remove(j);
                        }
                    }
                }

                dataBB.clear();

                for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddMF < MF; i++) {
                    if (i == arrSortirDataPlayerBB.size() - 1) {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                        r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                    } else {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                        r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    }
                    if (r > l) {
//                    System.out.println("MASUK R > L : " + arrSortirDataPlayerBB.get(i));
                        //arrSortirDataPlayerBB.remove(i);
                    } else {
                        dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                        countAddMF++;
                        totalWeightMF--;
                        pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                    }
                }

                for (int i = 0; i < dataBB.size(); i++) {
                    DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                    modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
                }
            }

        } else if (countGoodMF == 7 || countBadMF == 7) {
            int pickedValue = 0, l, r;
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 11; i < 18; i++) {
                    arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                }
            }
            //ub mf
            for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddMF < MF; i++) {
                if (i == arrSortirDataPlayerBB.size() - 1) {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                    r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                } else {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                }
                if (r > l) {
//                    System.out.println("MASUK R > L : " + arrSortirDataPlayerBB.get(i));
                    //arrSortirDataPlayerBB.remove(i);
                } else {
                    dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                    countAddMF++;
                    totalWeightMF--;
                    pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                }
            }

            for (int i = 0; i < dataBB.size(); i++) {
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
            }


            while (countAddMF < MF) {
                for (int i = 0; i < dataBB.size(); i++) {
                    String x = dataBB.get(i).getNama();
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getNama().equals(x)) {
                            arrSortirDataPlayerBB.remove(j);
                        }
                    }
                }

                dataBB.clear();
                for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddMF < MF; i++) {
                    if (i == arrSortirDataPlayerBB.size() - 1) {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                        r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                    } else {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                        r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    }
                    if (r > l) {
//                    System.out.println("MASUK R > L : " + arrSortirDataPlayerBB.get(i));
                        //arrSortirDataPlayerBB.remove(i);
                    } else {
                        dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                        countAddMF++;
                        totalWeightMF--;
                        pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                    }
                }

                for (int i = 0; i < dataBB.size(); i++) {
                    DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                    modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});

                }
            }


        } else {
            int pickedValue = 0, l, r;
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 11; i < 18; i++) {
                    if (arrAllDataPlayerBB.get(i).getFitness().equals("G")) {
                        arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                    }
                }
            }
            //ub MF
            for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddMF < MF; i++) {
                if (i == arrSortirDataPlayerBB.size() - 1) {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                    r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                } else {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                }
                if (r > l) {
                    //arrAllDataPlayerBB.remove(i);
                } else {
                    dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                    countAddMF++;
                    totalWeightMF--;
                    pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                }
            }

            for (int i = 0; i < dataBB.size(); i++) {
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
            }

            while (countAddMF < MF) {
                for (int i = 0; i < dataBB.size(); i++) {
                    String x = dataBB.get(i).getNama();
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getNama().equals(x)) {

                            arrSortirDataPlayerBB.remove(j);
                        }
                    }
                }

                dataBB.clear();

                for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddMF < MF; i++) {
                    if (i == arrSortirDataPlayerBB.size() - 1) {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                        r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                    } else {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightMF - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                        r = (pickedValue) + ((totalWeightMF - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    }
                    if (r > l) {
//                    System.out.println("MASUK R > L : " + arrSortirDataPlayerBB.get(i));
                        //arrSortirDataPlayerBB.remove(i);
                    } else {
                        dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                        countAddMF++;
                        totalWeightMF--;
                        pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                    }
                }

                for (int i = 0; i < dataBB.size(); i++) {
                    DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                    modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
                }
            }
        }
        countGoodMF = 0;
        countBadMF = 0;
        arrSortirDataPlayerBB.clear();
        dataBB.clear();

        //SORTING FW
        if (countGoodFW <= FW) {
            int pickedValue = 0, UB = 0, l, r;
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 18; i < 23; i++) {
                    arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getFitness().equals("G")) {
                            if (j == arrSortirDataPlayerBB.size() - 1) {
                                l = (pickedValue + arrSortirDataPlayerBB.get(j).getSkills()) + ((totalWeightFW - 1) * arrSortirDataPlayerBB.get(j).getDensity());
                                r = (pickedValue) + ((totalWeightFW - 0) * arrSortirDataPlayerBB.get(j).getDensity());

                            } else {
                                l = (pickedValue + arrSortirDataPlayerBB.get(j).getSkills()) + ((totalWeightFW - 1) * arrSortirDataPlayerBB.get(j + 1).getDensity());
                                r = (pickedValue) + ((totalWeightFW - 0) * arrSortirDataPlayerBB.get(j + 1).getDensity());
                            }
                            dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(j).getNo(), arrSortirDataPlayerBB.get(j).getSkills(), arrSortirDataPlayerBB.get(j).getNama(), arrSortirDataPlayerBB.get(j).getPosisi(), arrSortirDataPlayerBB.get(j).getFitness(), arrSortirDataPlayerBB.get(j).getDensity()));
                            countAddFW++;
                            totalWeightFW--;
                            System.out.println("Data ditambah di DATABB : " + arrSortirDataPlayerBB.get(j).getNama());
                            pickedValue += arrSortirDataPlayerBB.get(j).getSkills();
                            arrSortirDataPlayerBB.remove(j);


                        }
                    }
                }

            }

            for (int i = 0; i < dataBB.size(); i++) {
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
                System.out.println("ADDED : " + dataBB.get(i).getNama());
            }
            
            while (countAddFW < FW) {
                for (int i = 0; i < dataBB.size(); i++) {
                    String x = dataBB.get(i).getNama();
                    for (int j = 0; j < arrSortirDataPlayerBB.size(); j++) {
                        if (arrSortirDataPlayerBB.get(j).getNama().equals(x)) {

                            arrSortirDataPlayerBB.remove(j);
                        }
                    }
                }

                dataBB.clear();

                for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddFW < FW; i++) {
                    if (i == arrSortirDataPlayerBB.size() - 1) {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightFW - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                        r = (pickedValue) + ((totalWeightFW - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                    } else {
                        l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightFW - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                        r = (pickedValue) + ((totalWeightFW - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    }
                    if (r > l) {
//                    System.out.println("MASUK R > L : " + arrSortirDataPlayerBB.get(i));
                        //arrSortirDataPlayerBB.remove(i);
                    } else {
                        dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                        countAddFW++;
                        totalWeightFW--;
                        pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                    }
                }

                for (int i = 0; i < dataBB.size(); i++) {
                    DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                    modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
                }
            }

        } else if (countGoodFW == 5 || countBadFW == 5) {
            int pickedValue = 0, l, r;
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 18; i < 23; i++) {
                    arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                }
            }
            //ub FW
            for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddFW < FW; i++) {
                if (i == arrSortirDataPlayerBB.size() - 1) {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightFW - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                    r = (pickedValue) + ((totalWeightFW - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                } else {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightFW - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    r = (pickedValue) + ((totalWeightFW - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                }
                if (r > l) {
                    //arrAllDataPlayerBB.remove(i);
                } else {
                    dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                    countAddFW++;
                    totalWeightFW--;
                    pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                }
            }

            for (int i = 0; i < dataBB.size(); i++) {
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
            }


        } else {
            int pickedValue = 0, l, r;
            if (arrSortirDataPlayerBB.isEmpty() == true) {
                for (int i = 18; i < 23; i++) {
                    if (arrAllDataPlayerBB.get(i).getFitness().equals("G")) {
                        arrSortirDataPlayerBB.add(arrAllDataPlayerBB.get(i));
                    }
                }
            }
            //ub FW
            for (int i = 0; i < arrSortirDataPlayerBB.size() && countAddFW < FW; i++) {
                if (i == arrSortirDataPlayerBB.size() - 1) {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightFW - 1) * arrSortirDataPlayerBB.get(i).getDensity());
                    r = (pickedValue) + ((totalWeightFW - 0) * arrSortirDataPlayerBB.get(i).getDensity());

                } else {
                    l = (pickedValue + arrSortirDataPlayerBB.get(i).getSkills()) + ((totalWeightFW - 1) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                    r = (pickedValue) + ((totalWeightFW - 0) * arrSortirDataPlayerBB.get(i + 1).getDensity());
                }
                if (r > l) {
                    //arrAllDataPlayerBB.remove(i);
                } else {
                    dataBB.add(new UBclass(r, arrSortirDataPlayerBB.get(i).getNo(), arrSortirDataPlayerBB.get(i).getSkills(), arrSortirDataPlayerBB.get(i).getNama(), arrSortirDataPlayerBB.get(i).getPosisi(), arrSortirDataPlayerBB.get(i).getFitness(), arrSortirDataPlayerBB.get(i).getDensity()));
                    countAddFW++;
                    totalWeightFW--;
                    pickedValue += arrSortirDataPlayerBB.get(i).getSkills();
                }
            }

            for (int i = 0; i < dataBB.size(); i++) {
                DefaultTableModel modelOutput = (DefaultTableModel) tabelOutputBB.getModel();
                modelOutput.addRow(new Object[]{dataBB.get(i).getNo(), dataBB.get(i).getNama(), dataBB.get(i).getPosisi(), dataBB.get(i).getSkills(), dataBB.get(i).getFitness(), dataBB.get(i).getDensity(), dataBB.get(i).getUpperBound()});
            }


        }
        countGoodFW = 0;
        countBadFW = 0;
        arrSortirDataPlayerBB.clear();
        dataBB.clear();



        //        } catch (Exception e) {
        //        }
    }
    
    
    public static int countAverage() {
        ArrayList<Integer> countAvg = new ArrayList<>();
        int avg = 0;
        for (int i = 0; i < 11; i++) {
            int skillsS = (Integer) tabelOutputBB.getModel().getValueAt(i, 3);
            countAvg.add(skillsS);
        }
        for (int i = 0; i < 11; i++) {
            avg += countAvg.get(i);
        }
        return (avg / 11);
    }
}
