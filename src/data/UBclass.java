/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

/**
 *
 * @author Jati
 */
public class UBclass {
    public int upperBound;
    public int no,skills;
    public String nama,posisi,fitness;
    public int density;

    public UBclass(int upperBound, int no, int skills, String nama, String posisi, String fitness, int density) {
        this.upperBound = upperBound;
        this.no = no;
        this.skills = skills;
        this.nama = nama;
        this.posisi = posisi;
        this.fitness = fitness;
        this.density = density;
    }

    public UBclass() {
    }

    public int getUpperBound() {
        return upperBound;
    }

    public void setUpperBound(int upperBound) {
        this.upperBound = upperBound;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getSkills() {
        return skills;
    }

    public void setSkills(int skills) {
        this.skills = skills;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getPosisi() {
        return posisi;
    }

    public void setPosisi(String posisi) {
        this.posisi = posisi;
    }

    public String getFitness() {
        return fitness;
    }

    public void setFitness(String fitness) {
        this.fitness = fitness;
    }

    public int getDensity() {
        return density;
    }

    public void setDensity(int density) {
        this.density = density;
    }

    @Override
    public String toString() {
        return "UBclass{" + "upperBound=" + upperBound + ", no=" + no + ", skills=" + skills + ", nama=" + nama + ", posisi=" + posisi + ", fitness=" + fitness + ", density=" + density + '}';
    }

    
}
